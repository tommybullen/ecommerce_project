import React, { useEffect, useState } from "react";
import { useRecoilValue } from "recoil";
import { menProdState, womenProdState } from "../state.js";
import { NavLink, useHistory } from "react-router-dom";

const RenderProducts = (props) => {
  const gender = props.gender;
  const category = props.category;
  const menProds = useRecoilValue(menProdState);
  const womenProds = useRecoilValue(womenProdState);
  const history = useHistory();

  const [products, setProducts] = useState([]);
  const [order, setOrder] = useState("bestsellers");

  useEffect(() => {
    let tempProds = [];
    if (gender === "men") {
      tempProds = menProds;
    } else {
      tempProds = womenProds;
    }

    if (category !== "all") {
      tempProds = tempProds.filter(
        (item) => item.category.category === category
      );
    }
    if (order === "bestsellers") {
      tempProds = [...tempProds].sort(
        (a, b) => b.timesPurchased - a.timesPurchased
      );
    }

    if (order === "priceHL") {
      tempProds = [...tempProds].sort((a, b) => b.price - a.price);
    }
    if (order === "priceLH") {
      tempProds = [...tempProds].sort((a, b) => a.price - b.price);
    }

    if (order === "alphabetical") {
      tempProds = [...tempProds].sort((a, b) => a.name.localeCompare(b.name));
    }
    setProducts(tempProds);
  }, [gender, category, menProds, womenProds, order]);

  return (
    <div>
      <button className="back" onClick={history.goBack}>
        Back
      </button>
      <div className="dropdown">
        <button className="dropbtn">Sort</button>
        <div className="dropdown-content">
          <div
            className={order === "bestsellers" ? "special" : null}
            onClick={() => setOrder("bestsellers")}
          >
            Bestsellers
          </div>
          <div
            className={order === "priceLH" ? "special" : null}
            onClick={() => setOrder("priceLH")}
          >
            Price low-high
          </div>
          <div
            className={order === "priceHL" ? "special" : null}
            onClick={() => setOrder("priceHL")}
          >
            Price high-low
          </div>
          <div
            className={order === "alphabetical" ? "special" : null}
            onClick={() => setOrder("alphabetical")}
          >
            Alphabetical
          </div>
        </div>
      </div>
      <div className="productsgrid">
        {products.map((item) => {
          return (
            <div className="products" key={item._id}>
              <NavLink
                to={`/product/single/${item._id}`}
                className="productlink"
              >
                <img alt={item.name} className="productimg" src={item.image} />
                <p>
                  {item.name.charAt(0).toUpperCase() +
                    item.name.slice(1).toLowerCase()}
                </p>
                <p>£{item.price.toFixed(2)}</p>
              </NavLink>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default RenderProducts;
