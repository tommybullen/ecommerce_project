import React from "react";
import Cart from "../containers/Cart.js";
import { StripeProvider, Elements } from "react-stripe-elements";

const Stripe = (props) => {
  return (
    <StripeProvider
      apiKey={
        "pk_test_51K05E0DByPIiK3P7DFQD06UpuG2yMUa9YD4R8jUPzyQRKEboOHD8tkglCwPN36Va3JbrZhFmlE9VBcowrTq68r3g00Fawu1UnN"
      }
    >
      <Elements>
        <Cart {...props} />
      </Elements>
    </StripeProvider>
  );
};

export default Stripe;
