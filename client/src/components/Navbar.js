import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import logo from "../imgs/logo.png";
import { useRecoilValue } from "recoil";
import {
  menCatState,
  womenCatState,
  loginState,
  adminState,
  cartState,
} from "../state.js";

const Navbar = (props) => {
  const menCats = useRecoilValue(menCatState);
  const womenCats = useRecoilValue(womenCatState);
  const [showMenNav, setShowMenNav] = useState(false);
  const [showWomenNav, setShowWomenNav] = useState(false);
  const [showBurgerNav, setShowBurgerNav] = useState(false);

  const isLoggedIn = useRecoilValue(loginState);
  const isAdmin = useRecoilValue(adminState);

  const cart = useRecoilValue(cartState);
  const [totalItems, setTotalItems] = useState(0);

  useEffect(() => {
    if (cart) {
      let total = 0;
      let tempCart = cart;
      tempCart.forEach((item) => {
        total += item.quantity;
      });
      setTotalItems(total);
    }
  }, [cart]);

  const MenNavShow = () => {
    setShowMenNav(true);
  };
  const MenNavHide = () => {
    setShowMenNav(false);
  };
  const WomenNavShow = () => {
    setShowWomenNav(true);
  };
  const WomenNavHide = () => {
    setShowWomenNav(false);
  };

  const BurgerNavShow = () => {
    setShowBurgerNav(true);
  };
  const BurgerNavHide = () => {
    setShowBurgerNav(false);
  };

  return (
    <div>
      <div className="big">
        <div className="logobar" style={sty.logobar}>
          <NavLink exact to={"/"}>
            <img alt="logo" style={sty.logo} src={logo} />
          </NavLink>
          {!isLoggedIn ? (
            [
              <NavLink
                exact
                style={sty.default}
                activeStyle={sty.active}
                to="/register"
                key="link1"
              >
                Register
              </NavLink>,
              <NavLink
                exact
                style={sty.default}
                activeStyle={sty.active}
                to="/login"
                key="link2"
              >
                Login
              </NavLink>,
            ]
          ) : (
            <NavLink
              exact
              to="/"
              style={sty.default}
              activeStyle={sty.active}
              onClick={() => {
                props.logout();
              }}
              key="link3"
            >
              Logout
            </NavLink>
          )}

          {isLoggedIn && (
            <NavLink
              exact
              to="/profile"
              style={sty.default}
              activeStyle={sty.active}
            >
              Profile
            </NavLink>
          )}
          {totalItems ? (
            <NavLink
              exact
              to="/cart"
              style={sty.default}
              activeStyle={sty.active}
              key="link4"
            >
              Bag (<b>{totalItems}</b>)
            </NavLink>
          ) : (
            <NavLink
              exact
              to="/cart"
              style={sty.default}
              activeStyle={sty.active}
              key="link4"
            >
              Bag
            </NavLink>
          )}
          {isAdmin && (
            <NavLink
              exact
              to="/admin"
              style={sty.default}
              activeStyle={sty.active}
              key="link6"
            >
              Admin
            </NavLink>
          )}
        </div>
        <div className="navbar">
          <NavLink
            onMouseEnter={MenNavShow}
            onMouseLeave={MenNavHide}
            id="menlink"
            className={showMenNav ? "onMen" : "offMen"}
            exact
            to="/gender/men"
          >
            Men
          </NavLink>

          <NavLink
            onMouseEnter={WomenNavShow}
            onMouseLeave={WomenNavHide}
            className={showWomenNav ? "onMen" : "offMen"}
            exact
            to="/gender/women"
          >
            Women
          </NavLink>
        </div>
        <div
          className={showMenNav ? "showNav" : "noNav"}
          onMouseEnter={MenNavShow}
          onMouseLeave={MenNavHide}
        >
          {menCats.map((item, idx) => {
            return (
              <NavLink
                className="navlink"
                to={`/category/men/${item}`}
                key={idx}
              >
                {item.charAt(0).toUpperCase() + item.slice(1)}
              </NavLink>
            );
          })}
        </div>
        <div
          className={showWomenNav ? "showNav" : "noNav"}
          onMouseEnter={WomenNavShow}
          onMouseLeave={WomenNavHide}
        >
          {womenCats.map((item, idx) => {
            return (
              <NavLink
                className="navlink"
                exact
                to={`/category/women/${item}`}
                key={idx}
              >
                {item.charAt(0).toUpperCase() + item.slice(1)}
              </NavLink>
            );
          })}
        </div>
      </div>
      {/* ----------SMALL NAVIGATION----------- */}
      <div className="small">
        <div className="logobar" style={sty.logobar}>
          <span
            className="burger"
            onMouseEnter={BurgerNavShow}
            onMouseLeave={BurgerNavHide}
          >
            |||
          </span>
          <NavLink exact to={"/"}>
            <img alt="logo" style={sty.logo} src={logo} />
          </NavLink>

          {!isLoggedIn ? (
            [
              <NavLink
                exact
                style={sty.default}
                activeStyle={sty.active}
                to="/register"
                key="link1"
              >
                Register
              </NavLink>,
              <NavLink
                exact
                style={sty.default}
                activeStyle={sty.active}
                to="/login"
                key="link2"
              >
                Login
              </NavLink>,
            ]
          ) : (
            <NavLink
              exact
              to="/"
              style={sty.default}
              activeStyle={sty.active}
              onClick={() => {
                props.logout();
              }}
              key="link3"
            >
              Logout
            </NavLink>
          )}

          {isLoggedIn && (
            <NavLink
              exact
              to="/profile"
              style={sty.default}
              activeStyle={sty.active}
            >
              Profile
            </NavLink>
          )}
          {totalItems ? (
            <NavLink
              exact
              to="/cart"
              style={sty.default}
              activeStyle={sty.active}
              key="link4"
            >
              Bag (<b>{totalItems}</b>)
            </NavLink>
          ) : (
            <NavLink
              exact
              to="/cart"
              style={sty.default}
              activeStyle={sty.active}
              key="link4"
            >
              Bag
            </NavLink>
          )}
          {isAdmin && (
            <NavLink
              exact
              to="/admin"
              style={sty.default}
              activeStyle={sty.active}
              key="link6"
            >
              Admin
            </NavLink>
          )}
        </div>
        {/* --------HAMBURGER NAVBAR-------- */}
        <div className={showBurgerNav ? "flexnav" : "smallNoNav"}>
          <div
            className="smallnavbar"
            onMouseEnter={BurgerNavShow}
            onMouseLeave={BurgerNavHide}
          >
            <NavLink
              onMouseEnter={MenNavShow}
              onMouseLeave={MenNavHide}
              id="menlink"
              className={showMenNav ? "onMen" : "offMen"}
              exact
              to="/gender/men"
            >
              Men
            </NavLink>

            <NavLink
              onMouseEnter={WomenNavShow}
              onMouseLeave={WomenNavHide}
              className={showWomenNav ? "onMen" : "offMen"}
              exact
              to="/gender/women"
            >
              Women
            </NavLink>
          </div>
          <div>
            <div
              onMouseEnter={() => {
                MenNavShow();
                BurgerNavShow();
              }}
              onMouseLeave={() => {
                MenNavHide();
                BurgerNavHide();
              }}
              className={showMenNav ? "smallShowNav" : "smallNoNav"}
            >
              {menCats.map((item, idx) => {
                return (
                  <NavLink
                    className="navlink"
                    to={`/category/men/${item}`}
                    key={idx}
                  >
                    {item.charAt(0).toUpperCase() + item.slice(1)}
                  </NavLink>
                );
              })}
            </div>
            <div
              className={showWomenNav ? "smallShowNav" : "smallNoNav"}
              onMouseEnter={() => {
                WomenNavShow();
                BurgerNavShow();
              }}
              onMouseLeave={() => {
                WomenNavHide();
                BurgerNavHide();
              }}
            >
              {womenCats.map((item, idx) => {
                return (
                  <NavLink
                    className="navlink"
                    exact
                    to={`/category/women/${item}`}
                    key={idx}
                  >
                    {item.charAt(0).toUpperCase() + item.slice(1)}
                  </NavLink>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;

const sty = {
  active: {
    color: "gray",
  },
  default: {
    textDecoration: "none",
    color: "white",
  },
  logo: {
    height: "100%",
    width: "15vw",
    minWidth: "100px",
  },
};
