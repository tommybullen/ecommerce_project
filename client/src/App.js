import React, { useEffect } from "react";
import axios from "axios";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import Home from "./containers/Home.js";
import Login from "./containers/Login.js";
import Register from "./containers/Register.js";
import Admin from "./containers/Admin.js";
import EditUsers from "./containers/EditUsers.js";
import EditProducts from "./containers/EditProducts.js";
import Orders from "./containers/Orders.js";
import Navbar from "./components/Navbar.js";
import MenWomen from "./containers/MenWomen";
import Category from "./containers/Category";
import Product from "./containers/Product";
import Stripe from "./components/Stripe";
import Profile from "./containers/Profile.js";
import PaymentSuccess from "./containers/PaymentSuccess";
import PaymentError from "./containers/PaymentError";
import ForgotPassword from "./containers/ForgotPassword";
import { URL } from "./config";
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";
import {
  prodState,
  menProdState,
  womenProdState,
  menCatState,
  womenCatState,
  loginState,
  adminState,
  cartState,
} from "./state.js";

function App() {
  const token = JSON.parse(localStorage.getItem("token"));
  const prods = useRecoilValue(prodState);
  const setMenProds = useSetRecoilState(menProdState);
  const setWomenProds = useSetRecoilState(womenProdState);
  const setMenCats = useSetRecoilState(menCatState);
  const setWomenCats = useSetRecoilState(womenCatState);
  const [isLoggedIn, setIsLoggedIn] = useRecoilState(loginState);
  const [isAdmin, setIsAdmin] = useRecoilState(adminState);
  const cart = useRecoilValue(cartState);

  useEffect(() => {
    setMenProds(
      prods.filter((item) => item.gender === "men" || item.gender === "any")
    );
    setWomenProds(
      prods.filter((item) => item.gender === "women" || item.gender === "any")
    );
    setMenCats([
      ...new Set(
        prods
          .filter((item) => item.gender === "men" || item.gender === "any")
          .map((item) => item.category.category)
      ),
    ]);
    setWomenCats([
      ...new Set(
        prods
          .filter((item) => item.gender === "women" || item.gender === "any")
          .map((item) => item.category.category)
      ),
    ]);
  });

  useEffect(() => {
    const saveCart = () => {
      localStorage.setItem("cart", JSON.stringify(cart));
    };
    saveCart();
  }, [cart]);

  useEffect(() => {
    const verify_token = async () => {
      if (token === null) return setIsLoggedIn("");
      try {
        axios.defaults.headers.common["Authorization"] = token;
        const response = await axios.post(`${URL}/users/verify_token`);
        return response.data.ok
          ? login(token, response.data.succ.admin, response.data.succ._id)
          : logout();
      } catch (error) {
        console.log(error);
      }
    };
    verify_token();
  });

  const login = (token, admin, id) => {
    localStorage.setItem("token", JSON.stringify(token));
    setIsLoggedIn(id);
    if (admin) {
      setIsAdmin(true);
    }
  };
  const logout = () => {
    localStorage.removeItem("token");
    setIsLoggedIn("");
    setIsAdmin(false);
  };
  return (
    <div>
      <Router>
        <Navbar logout={logout} />
        <Route exact path="/" render={(props) => <Home {...props} />} />
        <Route
          exact
          path="/login"
          render={(props) =>
            isLoggedIn ? (
              <Redirect to={"/"} />
            ) : (
              <Login login={login} {...props} />
            )
          }
        />
        <Route
          exact
          path="/register"
          render={(props) =>
            isLoggedIn ? <Redirect to={"/"} /> : <Register {...props} />
          }
        />
        <Route exact path="/cart" render={(props) => <Stripe {...props} />} />
        <Route
          exact
          path="/profile"
          render={(props) =>
            !isLoggedIn ? <Redirect to={"/login"} /> : <Profile {...props} />
          }
        />
        <Route
          exact
          path="/admin"
          render={() =>
            !isLoggedIn || !isAdmin ? <Redirect to={"/"} /> : <Admin />
          }
        />
        <Route
          exact
          path="/admin/editusers"
          render={() =>
            !isLoggedIn || !isAdmin ? <Redirect to={"/"} /> : <EditUsers />
          }
        />
        <Route
          exact
          path="/admin/editproducts"
          render={() =>
            !isLoggedIn || !isAdmin ? <Redirect to={"/"} /> : <EditProducts />
          }
        />
        <Route
          exact
          path="/admin/orders"
          render={() =>
            !isLoggedIn || !isAdmin ? <Redirect to={"/"} /> : <Orders />
          }
        />
        <Route exact path="/product/single/:productid" component={Product} />
        <Route
          exact
          path="/gender/:gender"
          render={(props) => <MenWomen {...props} />}
        />
        <Route exact path="/category/:gender/:category" component={Category} />
        <Route
          exact
          path="/payment/success"
          render={(props) => <PaymentSuccess user={isLoggedIn} {...props} />}
        />
        <Route
          exact
          path="/payment/error"
          render={(props) => <PaymentError {...props} />}
        />
        <Route
          exact
          path="/forgot/:token"
          render={(props) => <ForgotPassword {...props} />}
        />
      </Router>
    </div>
  );
}

export default App;
