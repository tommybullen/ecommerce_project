import { selector } from "recoil";
import { atom } from "recoil";
import axios from "axios";
import { URL } from "./config.js";

export const prodState = selector({
  key: "products",
  get: async ({ get }) => {
    const products = await axios.get(`${URL}/products/products`);
    return products.data.data;
  },
});

export const catState = selector({
  key: "categories",
  get: async ({ get }) => {
    const categories = await axios.get(`${URL}/products/categories`);
    return categories.data.data;
  },
});

export const menProdState = atom({
  key: "menProducts",
  default: [],
});

export const womenProdState = atom({
  key: "womenProducts",
  default: [],
});

export const menCatState = atom({
  key: "menCategories",
  default: [],
});

export const womenCatState = atom({
  key: "womenCategories",
  default: [],
});

export const loginState = atom({
  key: "isLoggedIn",
  default: "",
});

export const adminState = atom({
  key: "isAdmin",
  default: false,
});

export const cartState = atom({
  key: "cart",
  default: JSON.parse(localStorage.getItem("cart")) || [],
});
