import React, { useState } from "react";
import { useRecoilValue } from "recoil";
import { prodState, menCatState, womenCatState } from "../state.js";
import { NavLink } from "react-router-dom";

const Home = () => {
  const prods = useRecoilValue(prodState);
  const menCats = useRecoilValue(menCatState);
  const womenCats = useRecoilValue(womenCatState);

  var menCatSales = [];
  menCats.forEach((cat, i) => {
    menCatSales.push({ category: cat, sales: 0 });
    prods.forEach((prod) => {
      if (
        prod.category.category === cat &&
        (prod.gender === "men" || prod.gender === "any")
      ) {
        menCatSales[i].sales += prod.timesPurchased;
      }
    });
  });

  var womenCatSales = [];
  womenCats.forEach((cat, i) => {
    womenCatSales.push({ category: cat, sales: 0 });
    prods.forEach((prod) => {
      if (
        prod.category.category === cat &&
        (prod.gender === "women" || prod.gender === "any")
      ) {
        womenCatSales[i].sales += prod.timesPurchased;
      }
    });
  });
  let topMenCats = [...menCatSales]
    .sort((a, b) => b.sales - a.sales)
    .filter((item, idx) => idx < 3);

  let topWomenCats = [...womenCatSales]
    .sort((a, b) => b.sales - a.sales)
    .filter((item, idx) => idx < 3);

  const [prodIdx, setProdIdx] = useState(0);

  let topProds = [...prods]
    .sort((a, b) => b.timesPurchased - a.timesPurchased)
    .filter((item, idx) => idx < 6);

  const handleClick = (e) => {
    e.persist();
    e.preventDefault();
    if (e.target.innerHTML === "&gt;") {
      if (prodIdx === topProds.length - 1) {
        setProdIdx(0);
      } else {
        setProdIdx(prodIdx + 1);
      }
    } else if (prodIdx === 0) {
      setProdIdx(topProds.length - 1);
    } else {
      setProdIdx(prodIdx - 1);
    }
  };

  return (
    <div className="main">
      <h1>Trending products</h1>
      <div className="carousel">
        <button className="carouselbuttons" id="lbutton" onClick={handleClick}>
          &lt;
        </button>
        <a href={`/product/single/${topProds[prodIdx]._id}`}>
          <img
            alt={topProds[prodIdx].name}
            className="carouselimg"
            src={topProds[prodIdx].image}
          />
        </a>
        <h3 className="carouselname">
          {topProds[prodIdx].name.charAt(0).toUpperCase() +
            topProds[prodIdx].name.slice(1)}
        </h3>

        <button
          className="carouselbuttons"
          id="rbutton"
          onClick={(e) => handleClick(e)}
        >
          &gt;
        </button>
      </div>
      <h1>Top categories</h1>
      <div className="homelinkscontainer">
        {topMenCats.map((item, idx) => {
          return (
            <NavLink
              className="catlinks"
              to={`/category/men/${item.category}`}
              key={idx}
            >
              Men's {item.category.toLowerCase()}
            </NavLink>
          );
        })}
        {topWomenCats.map((item, idx) => {
          return (
            <NavLink
              className="catlinks"
              to={`/category/women/${item.category}`}
              key={idx}
            >
              Women's {item.category.toLowerCase()}
            </NavLink>
          );
        })}
      </div>
    </div>
  );
};

export default Home;
