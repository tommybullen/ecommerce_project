import React, { useState } from "react";
import axios from "axios";
import { URL } from "../config";

const Login = (props) => {
  const [message, setMessage] = useState("");
  const [form, setValues] = useState({
    email: "",
    password: "",
  });

  const handleChange = (e) => {
    setValues({ ...form, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${URL}/users/login`, {
        email: form.email.toLowerCase(),
        password: form.password,
      });
      setMessage(response.data.message);
      console.log(response);
      if (response.data.ok) {
        if (response.data.admin)
          setTimeout(() => {
            props.login(
              response.data.token,
              response.data.admin,
              response.data.id
            );
            props.history.push("/admin");
          }, 2000);
        else {
          setTimeout(() => {
            props.login(
              response.data.token,
              response.data.admin,
              response.data.id
            );
            props.history.push("/");
          }, 2000);
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  const forgotPassword = async (e) => {
    e.preventDefault();
    if (form.email) {
      try {
        const res = await axios.post(`${URL}/users/forgot`, {
          email: form.email.toLowerCase(),
        });
        console.log(res.data);
        sendEmail(res.data);
        setMessage("A password reset link has been emailed to you");
      } catch (e) {
        console.log(e);
      }
    } else setMessage("Please enter your email");
  };

  const sendEmail = async (data) => {
    try {
      const res = await axios.post(`${URL}/emails/send_email`, {
        email: data.email,
        subject: "Password reset link",
        message: `Hi ${data.firstName}, please click <a href="http://localhost:3000/forgot/${data.token}">this link</a> to reset your password.`,
      });
      console.log(res);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div className="main">
      <form
        onSubmit={handleSubmit}
        onChange={handleChange}
        className="form_container"
      >
        <label>Email</label>
        <input name="email" />
        <label>Password</label>
        <input name="password" />
        <div>
          <button className="loginbtn">Login</button>
          <button onClick={forgotPassword} className="loginbtn">
            Forgot password
          </button>
        </div>
        <div className="message">
          <h4>{message}</h4>
        </div>
      </form>
    </div>
  );
};

export default Login;
