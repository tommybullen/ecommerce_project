import React, { useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import { useRecoilValue, useRecoilState } from "recoil";
import { prodState, cartState } from "../state.js";

const Product = () => {
  const { productid } = useParams();
  const prods = useRecoilValue(prodState);
  const [quantity, setQuantity] = useState(1);
  const [cart, setCart] = useRecoilState(cartState);
  const [message, setMessage] = useState("");
  const history = useHistory();

  var product = prods.find((item) => item._id === productid);

  const add = (e) => {
    e.preventDefault();

    if (quantity > 0) {
      let tempCart = [...cart];
      let index = tempCart.findIndex(
        (item) => item.product._id === product._id
      );
      if (index === -1) {
        tempCart.push({ product: product, quantity: quantity });
      } else {
        let item = { ...tempCart[index] };
        item.quantity += quantity;
        tempCart[index] = item;
      }
      setCart(tempCart);
      setMessage(`${quantity} added to bag`);
      setQuantity(1);
    }
    console.log("CAART", cart);
  };

  return (
    <div className="main">
      <h1>
        {product.name.charAt(0).toUpperCase() +
          product.name.slice(1).toLowerCase()}
      </h1>
      <button className="backprod" onClick={history.goBack}>
        Back
      </button>
      <div className="productcontainer">
        <img
          alt={product.name}
          className="singleproductimg"
          src={product.image}
        />
        <div className="productdetails">
          <h2>£{product.price.toFixed(2)}</h2>
          <p>{product.description}</p>
          <div>
            <p className="quantitylabel"> Quantity </p>
            <div className="addtobagbuttons">
              <button
                className="productbtn"
                onClick={() => quantity > 1 && setQuantity(quantity - 1)}
              >
                -
              </button>
              <p className="quantity">{quantity}</p>
              <button
                className="productbtn"
                onClick={() => setQuantity(quantity + 1)}
              >
                +
              </button>
              <button className="addtobagbtn" onClick={add}>
                Add to bag
              </button>
              <p className="quantity">{message}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Product;
