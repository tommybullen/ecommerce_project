import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { URL } from "../config";
import axios from "axios";
import { cartState } from "../state";
import { useSetRecoilState } from "recoil";

const PaymentSuccess = (props) => {
  const [cart, setCart] = useState([]);
  const [sessionId, setSessionId] = useState("");
  const [paymentMethod, setPaymentMethod] = useState("");
  const setGlobalCart = useSetRecoilState(cartState);

  useEffect(() => {
    if (sessionId && paymentMethod) {
      createOrder(cart, sessionId, paymentMethod);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.user, sessionId, paymentMethod]);

  useEffect(() => {
    const getSessionData = async () => {
      try {
        const tempSessionId = JSON.parse(localStorage.getItem("sessionId"));
        setSessionId(tempSessionId);
        const test = JSON.parse(localStorage.getItem("cart"));
        setCart(test);
        const response = await axios.get(
          `${URL}/payment/checkout-session?sessionId=${tempSessionId}`
        );
        localStorage.removeItem("sessionId");
        localStorage.removeItem("cart");
        setGlobalCart([]);
        setPaymentMethod(response.data.session.payment_method_types[0]);
      } catch (error) {
        console.log(error);
      }
    };
    getSessionData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const createOrder = async (cart, sessionId, paymentMethod) => {
    let order = {
      products: [],
      paymentMethod: paymentMethod,
      sessionId: sessionId,
      userId: props.user,
    };
    cart.forEach((item) => {
      order.products.push({
        productId: item.product._id,
        name: item.product.name,
        price: item.product.price,
        quantity: item.quantity,
      });
    });
    try {
      const res = await axios.post(`${URL}/orders/addorder`, order);
      console.log(res);
      updateSales(order.products);
      sendEmail();
    } catch (error) {
      console.log(error);
    }
  };

  const updateSales = async (products) => {
    try {
      let arr = [];
      products.forEach((item) => {
        console.log("ITEM.ID", item.productId);
        arr.push(
          axios.post(`${URL}/products/addsales`, {
            _id: item.productId,
            quantity: item.quantity,
          })
        );
      });
      Promise.all(arr).then((values) => {
        console.log(values);
      });
    } catch (err) {
      console.log(err);
    }
  };

  const sendEmail = async () => {
    try {
      const res = await axios.get(`${URL}/users/single/${props.user}`);
      var user = res.data.data;
    } catch (e) {
      console.log(e);
    }
    try {
      var total = 0;
      const map = `${cart.map((item) => {
        total += item.product.price * item.quantity;
        return `<ul><li>${
          item.product.name.charAt(0).toUpperCase() + item.product.name.slice(1)
        }</li><li>£${item.product.price.toFixed(2)}</li><li>Quantity: ${
          item.quantity
        }</li></ul>`;
      })}`;
      const tempMessage = `Hi ${
        user.firstName
      }, your order has been successful. Here are the details: ${map
        .split(",")
        .join("")}Total: £${total.toFixed()}`;
      console.log(tempMessage);
      const res = await axios.post(`${URL}/emails/send_email`, {
        email: user.email,
        subject: "Order confirmation",
        message: tempMessage,
      });
      console.log(res);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div className="main">
      <h1>Payment successful</h1>
      <h3>Thanks for your order!</h3>
      <NavLink className="button" exact to="/">
        Continue shopping
      </NavLink>
    </div>
  );
};

export default PaymentSuccess;
