import React from "react";
import { useParams } from "react-router-dom";
import RenderProducts from "../components/RenderProducts";
import { useRecoilValue } from "recoil";
import { menCatState, womenCatState } from "../state.js";
import { NavLink } from "react-router-dom";

const MenWomen = (props) => {
  const { gender } = useParams();
  var categories = [];
  const menCats = useRecoilValue(menCatState);
  const womenCats = useRecoilValue(womenCatState);
  //use effect check gender

  if (gender === "men") {
    categories = menCats;
  } else if (gender === "women") {
    categories = womenCats;
  } else {
    props.history.push("/");
  }

  return (
    <div className="main">
      <h1>{gender.toUpperCase()}SWEAR</h1>
      {/* <h2>Categories</h2> */}
      <div className="catlinkscontainer">
        {categories.map((item, idx) => {
          return (
            <NavLink
              className="catlinks"
              to={`/category/${gender}/${item}`}
              key={idx}
            >
              {item.charAt(0).toUpperCase() + item.slice(1)}
            </NavLink>
          );
        })}
      </div>
      <RenderProducts gender={gender} category="all" />
    </div>
  );
};

export default MenWomen;
