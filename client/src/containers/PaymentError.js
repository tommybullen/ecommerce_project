import React from "react";
import { NavLink } from "react-router-dom";

const PaymentError = () => {
  return (
    <div className="main">
      <h1>Payment failed</h1>
      <NavLink className="button" exact to="/cart">
        Return to shopping bag to try again
      </NavLink>
    </div>
  );
};

export default PaymentError;
