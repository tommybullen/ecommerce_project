import React from "react";
import { NavLink } from "react-router-dom";

const Admin = (props) => {
  return (
    <div className="main">
      <h1>Admin</h1>
      <div className="adminbuttoncontainer">
        <NavLink to="/admin/editusers" className="adminbutton">
          <h3 className="adminlabel">Edit users </h3>
        </NavLink>
        <NavLink to="/admin/editproducts" className="adminbutton">
          <h3 className="adminlabel">Edit products </h3>
        </NavLink>
        <NavLink to="/admin/orders" className="adminbutton">
          <h3 className="adminlabel">View orders </h3>
        </NavLink>
      </div>
    </div>
  );
};

export default Admin;
