import axios from "axios";
import React, { useState } from "react";
import { URL } from "../config";
import { useRecoilValue } from "recoil";
import { catState, prodState } from "../state.js";
import CategoryEditor from "./CategoryEditor";
import ProductEditor from "./ProductEditor";
import widgetStyle from "./widgetStyle";

const EditProducts = () => {
  const [newCat, setNewCat] = useState("");
  const [catMessage, setCatMessage] = useState("");
  const cats = useRecoilValue(catState);
  const prods = useRecoilValue(prodState);

  const [newProd, setNewProd] = useState({});
  const [prodMessage, setProdMessage] = useState("");

  const handleCatChange = (e) => {
    e.persist();
    setNewCat(e.target.value.toLowerCase());
  };

  const handleCatSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${URL}/products/addcategory`, {
        category: newCat,
      });
      console.log(newCat);
      setCatMessage(response.data.message);
    } catch (error) {
      console.log(error);
    }
  };

  const handleProdChange = (e) => {
    e.persist();
    let tempProd = newProd;
    e.target.id === "description"
      ? (tempProd[e.target.id] = e.target.value)
      : (tempProd[e.target.id] = e.target.value.toLowerCase());
    setNewProd(tempProd);
  };

  const uploadWidget = (e) => {
    e.preventDefault();
    let tempProd = newProd;
    window.cloudinary.openUploadWidget(
      {
        cloud_name: process.env.REACT_APP_CLOUD_NAME,
        upload_preset: process.env.REACT_APP_UPLOAD_PRESET,
        tags: ["user"],
        stylesheet: widgetStyle,
      },
      (error, result) => {
        if (error) {
          console.log(error);
        } else {
          tempProd.image = result[0].secure_url;
          tempProd.imagePublicId = result[0].public_id;
          setNewProd(tempProd);
          console.log("!!!", tempProd);
        }
      }
    );
  };

  const handleProdSubmit = async (e) => {
    e.preventDefault();
    console.log(newProd);
    try {
      const response = await axios.post(`${URL}/products/addproduct`, {
        product: newProd,
      });
      console.log(response.data);
      setProdMessage(response.data.message);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="main">
      <h1>Edit products</h1>
      <h2>Add new category</h2>
      <form>
        <input
          className="userinput"
          onChange={handleCatChange}
          placeholder="Enter new category"
        />
        <button className="smallbtn" onClick={handleCatSubmit}>
          Submit
        </button>
        <p>{catMessage}</p>
      </form>
      <h2>All categories</h2>
      <div className="grid">
        {cats.map((item) => {
          return <CategoryEditor key={item._id} item={item} />;
        })}
      </div>
      <h2>Add new product</h2>
      <form>
        <select
          onChange={handleProdChange}
          className="userinput"
          key="gender"
          id="gender"
          // value="Select gender"
          // placeholder="Select gender"
          defaultValue="Select gender"
        >
          <option value="Select gender" disabled>
            Select gender
          </option>
          <option value="men">Men</option>
          <option value="women">Women</option>
          <option value="any">Any</option>
        </select>

        <select
          onChange={handleProdChange}
          className="userinput"
          key="category"
          id="category"
        >
          <option value="" disabled>
            Select category
          </option>
          {cats.map((item) => {
            return (
              <option key={item._id} value={item._id}>
                {item.category.charAt(0).toUpperCase() + item.category.slice(1)}
              </option>
            );
          })}
        </select>
        <input
          onChange={handleProdChange}
          placeholder="Enter product name"
          className="userinput"
          key="name"
          id="name"
        />
        <input
          onChange={handleProdChange}
          placeholder="Enter product price"
          className="userinput"
          key="price"
          id="price"
        />
        <input
          onChange={handleProdChange}
          placeholder="Enter product colour"
          className="userinput"
          key="colour"
          id="colour"
        />
        <input
          onChange={handleProdChange}
          placeholder="Enter product description"
          className="userinput"
          key="description"
          id="description"
        />
        <button className="smallbtn" onClick={uploadWidget}>
          Upload image
        </button>
        <button className="smallbtn" onClick={handleProdSubmit}>
          Submit
        </button>
        <p className="productmessage">{prodMessage}</p>
      </form>
      <h2>All products</h2>
      <div className="grid">
        {prods.map((item) => {
          return <ProductEditor key={item._id} item={item} />;
        })}
      </div>
    </div>
  );
};

export default EditProducts;
