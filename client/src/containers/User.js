import React, { useState } from "react";
import axios from "axios";
import { URL } from "../config";

const User = (props) => {
  const item = props.item;
  const [editId, setEditId] = useState("");
  const [message, setMessage] = useState("");

  const initialValues = {
    email: item.email,
    password: item.password,
    firstName: item.firstName,
    lastName: item.lastName,
    street: item.street,
    town: item.town,
    county: item.county,
    postCode: item.postCode,
    phoneNumber: item.phoneNumber,
    admin: item.admin,
    dateCreated: item.dateCreated,
  };

  const [editValues, setEditValues] = useState(initialValues);

  const editUser = (id) => {
    setEditId(id);
  };

  const handleChange = (e) => {
    e.persist();
    let tempValues = editValues;
    tempValues[e.target.id] = e.target.value;
    setEditValues(tempValues);
  };

  const handleAdmin = () => {
    let tempValues = editValues;
    tempValues.admin = !tempValues.admin;
    setEditValues(tempValues);
  };

  const handleCancel = () => {
    setEditValues(initialValues);
    setEditId("");
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await axios.post(`${URL}/users/edit`, {
        initialValues: initialValues,
        editValues: editValues,
      });
      setMessage(res.data.data);
      handleCancel();
    } catch (err) {
      console.log(err);
    }
  };

  const deleteUser = async (e, id) => {
    e.preventDefault();
    try {
      const res = await axios.post(`${URL}/users/delete`, {
        _id: id,
      });
      console.log(res);
    } catch (err) {
      console.log(err);
    }
  };

  if (editId !== item._id) {
    return (
      <div key={item._id} className="allusercontainer">
        <div className="user">
          <p className="userinput"> {item.firstName}</p>
          <p className="userinput"> {item.lastName} </p>
          <p className="userinput">{item.street} </p>
          <p className="userinput">{item.town} </p>
          <p className="userinput">{item.county} </p>
          <p className="userinput">{item.postCode} </p>
          <p className="userinput">{item.email} </p>
          <p className="userinput">{item.phoneNumber} </p>
          {item.admin ? (
            <p className="userinput">&#10004; Admin user</p>
          ) : (
            <p className="userinput"> Not admin user</p>
          )}
        </div>
        <button className="smallbtn" onClick={() => editUser(item._id)}>
          Edit
        </button>
        <button className="smallbtn" onClick={(e) => deleteUser(e, item._id)}>
          Delete
        </button>
        <p>{message}</p>
      </div>
    );
  }
  return (
    <div key={item._id} className="allusercontainer">
      <div className="user">
        <input
          id="firstName"
          onChange={handleChange}
          className="userinput"
          placeholder={item.firstName}
        />
        <input
          id="lastName"
          onChange={handleChange}
          className="userinput"
          placeholder={item.lastName}
        />
        <input
          id="street"
          onChange={handleChange}
          className="userinput"
          placeholder={item.street}
        />
        <input
          id="town"
          onChange={handleChange}
          className="userinput"
          placeholder={item.town}
        />
        <input
          id="county"
          onChange={handleChange}
          className="userinput"
          placeholder={item.county}
        />
        <input
          id="postCode"
          onChange={handleChange}
          className="userinput"
          placeholder={item.postCode}
        />
        <input
          id="email"
          onChange={handleChange}
          className="userinput"
          placeholder={item.email}
        />
        <input
          id="phoneNumber"
          onChange={handleChange}
          className="userinput"
          placeholder={item.phoneNumber}
        />
        <div>
          <label className="userinput" id="admin" for="admin">
            Admin
          </label>
          {item.admin ? (
            <input
              onChange={handleAdmin}
              id="admin"
              type="checkbox"
              defaultChecked
              label="Admin"
            />
          ) : (
            <input onChange={handleAdmin} id="admin" type="checkbox" />
          )}
        </div>
      </div>
      <button className="smallbtn" onClick={handleCancel}>
        Cancel
      </button>
      <button className="smallbtn" onClick={handleSubmit}>
        Submit
      </button>
    </div>
  );
};

export default User;
