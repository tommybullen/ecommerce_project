import axios from "axios";
import React, { useEffect, useState } from "react";
import { URL } from "../config";

const Orders = () => {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    const getOrders = async () => {
      try {
        const response = await axios.get(`${URL}/orders/all`);
        console.log("RESPONSE; ", response);
        setOrders(response.data.data);
      } catch (err) {
        console.log(err);
      }
    };
    getOrders();
  }, []);

  return (
    <div className="main">
      <h1>Orders</h1>
      <div>
        {orders.map((item) => {
          let total = 0;
          return (
            <div className="order" key={item._id}>
              <b className="userinput">{item._id}</b>
              <p className="userinput">
                {item.dateCreated.substring(0, 10)}{" "}
                {item.dateCreated.substring(11, 16)}
              </p>
              <p className="userinput">
                {item.userId.firstName} {item.userId.lastName}
              </p>
              <p className="userinput">{item.userId.email}</p>
              <p className="userinput">Payment method: {item.paymentMethod}</p>
              <div className="orderprodwrapper">
                {item.products.map((ele) => {
                  console.log("ELE", ele);
                  total += ele.price * ele.quantity;
                  return (
                    <div className="orderprod" key={ele._id}>
                      <p className="userinput">
                        {ele.name.charAt(0).toUpperCase() + ele.name.slice(1)}
                      </p>
                      <p className="userinput">£{ele.price.toFixed(2)}</p>
                      <p className="userinput">Quantity: {ele.quantity}</p>
                      {ele.productId && (
                        <img
                          className="adminimg"
                          alt={ele.name}
                          src={ele.productId.image}
                        ></img>
                      )}
                    </div>
                  );
                })}
              </div>
              <div className="ordertotal">
                <p className="userinput">Total: £{total.toFixed(2)}</p>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Orders;
