import React from "react";
import { useParams } from "react-router-dom";
import RenderProducts from "../components/RenderProducts";

const Category = () => {
  const { gender } = useParams();
  const { category } = useParams();

  return (
    <div className="main">
      <h1>
        {gender.toUpperCase()}'S {category.toUpperCase()}
      </h1>
      <RenderProducts gender={gender} category={category} />
    </div>
  );
};

export default Category;
