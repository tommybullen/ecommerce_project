import React, { useState, useEffect } from "react";
import axios from "axios";
import { URL } from "../config";
import { useRecoilValue } from "recoil";
import { loginState } from "../state.js";

const Profile = () => {
  const userId = useRecoilValue(loginState);
  const [user, setUser] = useState([]);
  const [editing, setEditing] = useState(false);
  const [editValues, setEditValues] = useState([]);
  const [message, setMessage] = useState("");
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    const getUser = async () => {
      try {
        const res = await axios.get(`${URL}/users/single/${userId}`);
        setUser(res.data.data);
        setEditValues(res.data.data);
      } catch (e) {
        console.log(e);
      }
    };
    getUser();
  }, [userId]);

  const initialValues = {
    email: user.email,
    password: user.password,
    firstName: user.firstName,
    lastName: user.lastName,
    street: user.street,
    town: user.town,
    county: user.county,
    postCode: user.postCode,
    phoneNumber: user.phoneNumber,
    admin: user.admin,
    dateCreated: user.dateCreated,
  };

  const handleChange = (e) => {
    e.persist();
    let tempValues = editValues;
    tempValues[e.target.id] = e.target.value;
    setEditValues(tempValues);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await axios.post(`${URL}/users/edit`, {
        initialValues: initialValues,
        editValues: editValues,
      });
      setMessage(res.data.data);
      setEditing(false);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    const getOrders = async () => {
      try {
        const results = await axios.get(`${URL}/orders/user/${userId}`);
        setOrders(results.data.data);
      } catch (err) {
        console.log(err);
      }
    };
    getOrders();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  return (
    <div className="main">
      <h1>Profile</h1>
      <h2>Your details</h2>
      {!editing ? (
        <div className="profile">
          <div>
            <p>
              {user.firstName} {"  "}
              {user.lastName}
            </p>

            <div className="address">
              <p>{user.street} </p>
              <p>{user.town} </p>
              <p>{user.county} </p>
              <p>{user.postCode} </p>
            </div>
            <p>{user.email} </p>
            <p>{user.phoneNumber} </p>
            {user.admin && <p>&#10004; Admin user</p>}
          </div>
          <button className="cartbtn" onClick={() => setEditing(true)}>
            Edit
          </button>
          <p>{message}</p>
        </div>
      ) : (
        <div className="profileform" key={user._id}>
          <div>
            <input
              id="firstName"
              onChange={handleChange}
              className="profileinput"
              placeholder={user.firstName || "First name"}
            />
            <input
              id="lastName"
              onChange={handleChange}
              className="profileinput"
              placeholder={user.lastName || "Surname"}
            />
            <input
              id="street"
              onChange={handleChange}
              className="profileinput"
              placeholder={user.street || "Street"}
            />
            <input
              id="town"
              onChange={handleChange}
              className="profileinput"
              placeholder={user.town || "Town"}
            />
            <input
              id="county"
              onChange={handleChange}
              className="profileinput"
              placeholder={user.county || "County"}
            />
            <input
              id="postCode"
              onChange={handleChange}
              className="profileinput"
              placeholder={user.postCode || "Postcode"}
            />
            <input
              id="email"
              onChange={handleChange}
              className="profileinput"
              placeholder={user.email || "Email"}
            />
            <input
              id="phoneNumber"
              onChange={handleChange}
              className="profileinput"
              placeholder={user.phoneNumber || "Phone number"}
            />
          </div>
          <div className="profilebtns">
            <button className="paymentbtn" onClick={() => setEditing(false)}>
              Cancel
            </button>
            <button className="paymentbtn" onClick={handleSubmit}>
              Submit
            </button>
          </div>
        </div>
      )}
      {orders.length < 1 ? (
        <h2>You have no orders to show</h2>
      ) : (
        <div>
          <h2>Your orders</h2>
          {orders.map((item) => {
            let total = 0;
            return (
              <div className="order" key={item._id}>
                <b className="userinput">{item._id}</b>
                <p className="userinput">
                  {item.dateCreated.substring(0, 10)}{" "}
                  {item.dateCreated.substring(11, 16)}
                </p>
                <p className="userinput">
                  Payment method: {item.paymentMethod}
                </p>
                <div className="orderprodwrapper">
                  {item.products.map((ele) => {
                    total += ele.price * ele.quantity;
                    return (
                      <div className="orderprod" key={ele._id}>
                        <p className="userinput">
                          {ele.name.charAt(0).toUpperCase() + ele.name.slice(1)}
                        </p>
                        <p className="userinput">£{ele.price.toFixed(2)}</p>
                        <p className="userinput">Quantity: {ele.quantity}</p>
                        {ele.productId && (
                          <img
                            className="adminimg"
                            alt={ele.name}
                            src={ele.productId.image}
                          ></img>
                        )}
                      </div>
                    );
                  })}
                </div>
                <div className="ordertotal">
                  <p className="userinput">Total: £{total.toFixed(2)}</p>
                </div>
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};
export default Profile;
