import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { URL } from "../config";
import axios from "axios";
import { NavLink } from "react-router-dom";

const ForgotPassword = () => {
  const { token } = useParams();
  const [verified, setVerified] = useState(true);
  const [user, setUser] = useState([]);
  const [form, setValues] = useState({
    password: "",
    password2: "",
  });
  const [message, setMessage] = useState("");
  const [changed, setChanged] = useState(false);

  const handleChange = (e) => {
    setValues({ ...form, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    console.log(message);
  }, [message]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await axios.post(`${URL}/users/changepassword`, {
        _id: user._id,
        password: form.password,
        password2: form.password2,
      });
      console.log(res);
      setMessage(res.data.message);
      res.data.ok && setChanged(true);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    const verify_token = async () => {
      try {
        axios.defaults.headers.common["Authorization"] = token;
        const response = await axios.post(`${URL}/users/verify_token`);
        console.log(response.data);
        setUser(response.data.succ);
        !response.data.ok && setVerified(false);
      } catch (error) {
        console.log(error);
        setVerified(false);
      }
    };
    verify_token();
  }, [token]);

  return (
    <div className="main">
      <h1>Forgot password</h1>
      {verified ? (
        <div>
          {!changed ? (
            <form
              onSubmit={handleSubmit}
              onChange={handleChange}
              className="form_container"
            >
              <label>New password</label>
              <input name="password" />
              <label>Repeat new password</label>
              <input name="password2" />
              <button className="paymentbtn">Submit</button>
              <p>{message}</p>
            </form>
          ) : (
            <div className="form_container">
              <p>{message}</p>
              <NavLink className="paymentbtn" exact to="/login">
                Login
              </NavLink>
            </div>
          )}
        </div>
      ) : (
        <div>
          <p>Invalid link, please return to login page to try again.</p>
          <NavLink className="paymentbtn" exact to="/login">
            Login
          </NavLink>
        </div>
      )}
    </div>
  );
};

export default ForgotPassword;
