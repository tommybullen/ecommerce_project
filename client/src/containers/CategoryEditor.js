import React, { useState } from "react";
import axios from "axios";
import { URL } from "../config";

const CategoryEditor = (props) => {
  const item = props.item;

  const initialValue = {
    category: item.category,
  };

  const [catId, setCatId] = useState("");
  const [categoryValue, setCategoryValue] = useState(initialValue);
  const [message, setMessage] = useState("");

  const editCategory = (id) => {
    setCatId(id);
  };

  const handleChange = (e) => {
    e.persist();
    let tempValue = categoryValue;
    tempValue.category = e.target.value.toLowerCase();
    setCategoryValue(tempValue);
  };

  const handleCancel = () => {
    setCategoryValue(initialValue);
    setCatId("");
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await axios.post(`${URL}/products/editcategory`, {
        initialValue: initialValue,
        categoryValue: categoryValue,
      });
      setMessage(
        res.data.data.charAt(0).toUpperCase() + res.data.data.slice(1)
      );
      handleCancel();
    } catch (err) {
      console.log(err);
    }
  };

  const deleteCategory = async (e, id) => {
    e.preventDefault();
    try {
      const res = await axios.post(`${URL}/products/deletecategory`, {
        _id: id,
      });
      setMessage(res.data.data);
    } catch (err) {
      console.log(err);
    }
  };

  if (catId !== item._id) {
    return (
      <div key={item._id} className="allusercontainer">
        <p className="categorynames">
          {item.category.charAt(0).toUpperCase() + item.category.slice(1)}
        </p>
        <button onClick={() => editCategory(item._id)} className="smallbtn">
          Edit
        </button>
        <button
          onClick={(e) => deleteCategory(e, item._id)}
          className="smallbtn"
        >
          Delete
        </button>
        <p className="userinput">{message}</p>
      </div>
    );
  }
  return (
    <div key={item._id} className="allusercontainer">
      <input
        id=""
        onChange={handleChange}
        className="userinput"
        placeholder={
          item.category.charAt(0).toUpperCase() + item.category.slice(1)
        }
      />
      <button className="smallbtn" onClick={handleCancel}>
        Cancel
      </button>
      <button className="smallbtn" onClick={handleSubmit}>
        Submit
      </button>
    </div>
  );
};

export default CategoryEditor;
