import axios from "axios";
import React, { useEffect, useState } from "react";
import { URL } from "../config";
import User from "./User";

const EditUsers = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    const getUsers = async () => {
      try {
        const res = await axios.get(`${URL}/users/users`);
        setUsers(res.data.data);
      } catch (e) {
        console.log(e);
      }
    };
    getUsers();
  });

  const [newUser, setNewUser] = useState([]);
  const [message, setMessage] = useState("");

  const handleChange = (e) => {
    e.persist();
    let tempUser = newUser;
    tempUser[e.target.id] = e.target.value;
    console.log(tempUser);
    setNewUser(tempUser);
  };

  const handleAdmin = (e) => {
    e.persist();
    let tempUser = newUser;
    if (e.target.checked) {
      tempUser.admin = true;
    } else {
      tempUser.admin = false;
    }
    console.log(tempUser);
    setNewUser(tempUser);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${URL}/users/register`, {
        email: newUser.email,
        password: newUser.password,
        password2: newUser.password2,
        firstName: newUser.firstName,
        lastName: newUser.lastName,
        street: newUser.street,
        town: newUser.town,
        county: newUser.county,
        postCode: newUser.postCode,
        phoneNumber: newUser.phoneNumber,
        admin: newUser.admin,
      });
      setMessage(response.data.message);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="main">
      <h1>Edit users </h1>
      <h2>Add new user</h2>
      <form id="userform" className="usercontainer">
        <input
          key="email"
          className="userinput"
          id="email"
          onChange={handleChange}
          placeholder="Enter email"
        />
        <input
          key="password"
          className="userinput"
          id="password"
          onChange={handleChange}
          placeholder="Enter password"
        />
        <input
          key="password2"
          className="userinput"
          id="password2"
          onChange={handleChange}
          placeholder="Confirm password"
        />
        <input
          key="firstName"
          className="userinput"
          id="firstName"
          onChange={handleChange}
          placeholder="Enter first name"
        />
        <input
          key="lastName"
          className="userinput"
          id="lastName"
          onChange={handleChange}
          placeholder="Enter last name"
        />
        <input
          key="street"
          className="userinput"
          id="street"
          onChange={handleChange}
          placeholder="Enter street address"
        />
        <input
          key="town"
          className="userinput"
          id="town"
          onChange={handleChange}
          placeholder="Enter town"
        />
        <input
          key="county"
          className="userinput"
          id="county"
          onChange={handleChange}
          placeholder="Enter county"
        />
        <input
          key="postCode"
          className="userinput"
          id="postCode"
          onChange={handleChange}
          placeholder="Enter post code"
        />
        <input
          key="phoneNumber"
          className="userinput"
          id="phoneNumber"
          onChange={handleChange}
          placeholder="Enter phone number"
        />
        <div>
          <label
            key="adminlabel"
            className="userinput"
            id="admin"
            htmlFor="admin"
          >
            Admin
          </label>
          <input
            key="admininput"
            onChange={handleAdmin}
            id="admin"
            type="checkbox"
          />
        </div>
        <button className="smallbtn" onClick={handleSubmit}>
          {" "}
          Submit
        </button>
        <p>{message}</p>
      </form>
      <h2>All users</h2>
      <div className="grid">
        {users.map((item) => {
          return <User key={item._id} item={item} />;
        })}
      </div>
    </div>
  );
};

export default EditUsers;
