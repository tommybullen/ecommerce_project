import React from "react";
import axios from "axios";
import { useRecoilState, useRecoilValue } from "recoil";
import { cartState } from "../state.js";
import { loginState } from "../state.js";
import { NavLink } from "react-router-dom";
import { URL } from "../config";
import { injectStripe } from "react-stripe-elements";

const Cart = (props) => {
  const [cart, setCart] = useRecoilState(cartState);
  const isLoggedIn = useRecoilValue(loginState);

  const createCheckoutSession = async () => {
    try {
      const response = await axios.post(
        `${URL}/payment/create-checkout-session`,
        { cart }
      );
      console.log(response);
      return response.data.ok
        ? (localStorage.setItem(
            "sessionId",
            JSON.stringify(response.data.sessionId)
          ),
          localStorage.setItem("cart", JSON.stringify(cart)),
          redirect(response.data.sessionId))
        : props.history.push("/payment/error");
    } catch (error) {
      props.history.push("/payment/error");
    }
  };

  const redirect = (sessionId) => {
    props.stripe
      .redirectToCheckout({
        // Make the id field from the Checkout Session creation API response
        // available to this file, so you can provide it as parameter here
        // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
        sessionId: sessionId,
      })
      .then(function (result) {
        // If `redirectToCheckout` fails due to a browser or network
        // error, display the localized error message to your customer
        // using `result.error.message`.
      });
  };

  const editQuantity = (how, idx) => {
    const tempCart = [...cart];
    if (how === "add") {
      tempCart[idx] = {
        ...tempCart[idx],
        quantity: tempCart[idx].quantity + 1,
      };
    }
    if (how === "minus") {
      if (tempCart[idx].quantity > 1) {
        tempCart[idx] = {
          ...tempCart[idx],
          quantity: tempCart[idx].quantity - 1,
        };
        setCart(tempCart);
        return null;
      }
      if (tempCart[idx].quantity === 1) {
        tempCart.splice(idx, 1);
      }
    }
    setCart(tempCart);
  };

  let total = 0;

  return (
    <div className="main">
      <h1>Shopping bag</h1>
      {cart.length === 0 ? (
        <h3>Your shopping bag is empty</h3>
      ) : (
        <div className="cartitem smallnone">
          <p></p>
          <p></p>
          <p>
            <b>Price</b>
          </p>
          <p>
            <b>Quantity</b>
          </p>
          <p>
            <b>Total</b>
          </p>
        </div>
      )}
      {cart.map((item, idx) => {
        total += item.product.price * item.quantity;
        return (
          <div className="cartitem" key={item.product._id}>
            <img
              className="cartimg"
              alt={item.product.name}
              src={item.product.image}
            />
            <p className="productname">
              {item.product.name.charAt(0).toUpperCase() +
                item.product.name.slice(1)}
            </p>
            <p>£{item.product.price.toFixed(2)}</p>
            <div className="cartbtns">
              {item.quantity > 1 && (
                <button
                  className="productbtn"
                  onClick={() => editQuantity("minus", idx)}
                >
                  -
                </button>
              )}
              {item.quantity === 1 && (
                <button
                  className="cartbtn"
                  onClick={() => editQuantity("minus", idx)}
                >
                  Remove
                </button>
              )}

              <p>{item.quantity}</p>
              <button
                className="productbtn"
                onClick={() => editQuantity("add", idx)}
              >
                +
              </button>
            </div>
            <p className="totalhidden">Sub-total:</p>
            <p>£{(item.product.price * item.quantity).toFixed(2)}</p>
          </div>
        );
      })}
      {cart.length !== 0 && (
        <div className="cartitem">
          <p className="smallnone"></p>
          <p className="smallnone"></p>
          <p></p>
          {isLoggedIn ? (
            <button
              className="checkoutbtn"
              onClick={() => createCheckoutSession()}
            >
              Checkout
            </button>
          ) : (
            <NavLink className="button" exact to="/login">
              Log in to checkout
            </NavLink>
          )}

          <b className="total">£{total.toFixed(2)}</b>
        </div>
      )}
    </div>
  );
};

export default injectStripe(Cart);
