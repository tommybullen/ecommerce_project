import React, { useState } from "react";
import axios from "axios";
import { URL } from "../config";
import { useRecoilValue } from "recoil";
import { catState } from "../state.js";

const ProductEditor = (props) => {
  const item = props.item;
  const [productId, setProductId] = useState("");
  const [message, setMessage] = useState("");
  const cats = useRecoilValue(catState);

  const initialValues = {
    name: item.name,
    price: item.price,
    colour: item.colour,
    description: item.description,
    image: item.image,
    gender: item.gender,
    category: item.category,
    timesPurchased: item.timesPurchased,
    dateCreated: item.dateCreated,
  };
  const [editValues, setEditValues] = useState(initialValues);

  const editProduct = (id) => {
    setProductId(id);
  };

  const handleChange = (e) => {
    e.persist();
    let tempValues = editValues;
    tempValues[e.target.id] = e.target.value;
    setEditValues(tempValues);
  };

  const handleCancel = () => {
    setEditValues(initialValues);
    setProductId("");
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await axios.post(`${URL}/products/editproduct`, {
        initialValues: initialValues,
        editValues: editValues,
      });
      setMessage(
        res.data.data.charAt(0).toUpperCase() + res.data.data.slice(1)
      );
      handleCancel();
    } catch (err) {
      console.log(err);
    }
  };

  const deleteProduct = async (e, id) => {
    e.preventDefault();
    try {
      const res = await axios.post(`${URL}/products/deleteproduct`, {
        _id: id,
      });
      console.log("!!!", res);
      setMessage(res.data.data);
    } catch (err) {
      console.log(err);
    }
  };

  if (productId !== item._id) {
    return (
      <div key={item._id} className="allusercontainer">
        <div className="user">
          <p className="productname">
            {" "}
            <b>{item.name.charAt(0).toUpperCase() + item.name.slice(1)}</b>
          </p>
          <p className="userinput">
            Gender: <b>{item.gender.toLowerCase()}</b>
          </p>
          <p className="userinput">
            Category: <b>{item.category.category.toLowerCase()}</b>
          </p>
          <p className="userinput">
            Price: <b>£{item.price.toFixed(2)}</b>{" "}
          </p>
          <p className="userinput">
            Colour: <b>{item.colour.toLowerCase()}</b>
          </p>
          <p className="userinput">
            Times purchased: <b>{item.timesPurchased} </b>
          </p>
          <p className="description">
            {item.description.charAt(0).toUpperCase() +
              item.description.slice(1)}{" "}
          </p>
          <img alt={item.name} className="adminimg" src={item.image} />
        </div>
        <button className="smallbtn" onClick={() => editProduct(item._id)}>
          Edit
        </button>
        <button
          className="smallbtn"
          onClick={(e) => deleteProduct(e, item._id)}
        >
          Delete
        </button>
        <p className="productmessage">{message}</p>
      </div>
    );
  }
  return (
    <div key={item._id} className="allusercontainer">
      <div className="user">
        <input
          id="name"
          onChange={handleChange}
          className="userinput"
          placeholder={item.name}
        />
        <select
          onChange={handleChange}
          className="userinput"
          key="gender"
          id="gender"
        >
          <option value="" disabled selected>
            Select gender
          </option>
          <option value="men">Men</option>
          <option value="women">Women</option>
          <option value="any">Any</option>
        </select>
        <select
          onChange={handleChange}
          className="userinput"
          key="category"
          id="category"
        >
          <option value="" disabled selected>
            Select category
          </option>
          {cats.map((item) => {
            return (
              <option key={item._id} value={item._id}>
                {item.category.charAt(0).toUpperCase() + item.category.slice(1)}
              </option>
            );
          })}
        </select>

        <input
          id="price"
          onChange={handleChange}
          className="userinput"
          placeholder={item.price.toFixed(2)}
        />
        <input
          id="colour"
          onChange={handleChange}
          className="userinput"
          placeholder={item.colour}
        />
        <textarea
          id="description"
          onChange={handleChange}
          className="description"
          defaultValue={item.description}
          rows="5"
        ></textarea>
        <input
          id="image"
          onChange={handleChange}
          className="userinput"
          placeholder={item.image}
        />
      </div>
      <button className="smallbtn" onClick={handleCancel}>
        Cancel
      </button>
      <button className="smallbtn" onClick={handleSubmit}>
        Submit
      </button>
    </div>
  );
};

export default ProductEditor;
