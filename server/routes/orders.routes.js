const router = require("express").Router();
const controller = require("../controllers/orders.controllers.js");

router.post("/addorder", controller.addOrder);
router.get("/all", controller.allOrders);
router.get("/user/:id", controller.userOrders);

module.exports = router;
