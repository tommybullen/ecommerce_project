const express = require("express"),
  router = express.Router(),
  controller = require("../controllers/products.controllers");

router.get("/products", controller.allProducts);

router.get("/categories", controller.allCategories);

router.post("/addcategory", controller.addCategory);

router.post("/addproduct", controller.addProduct);

router.post("/editcategory", controller.editCategory);

router.post("/editproduct", controller.editProduct);

router.post("/deleteproduct", controller.deleteProduct);

router.post("/deletecategory", controller.deleteCategory);

router.post("/addsales", controller.addSales);

module.exports = router;
