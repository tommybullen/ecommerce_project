const router = require("express").Router();
const controller = require("../controllers/users.controllers");

router.post("/register", controller.register);
router.post("/login", controller.login);
router.post("/verify_token", controller.verify_token);
router.get("/users", controller.all_users);
router.post("/edit", controller.edit_user);
router.post("/delete", controller.deleteUser);
router.get("/single/:id", controller.findOne);
router.post("/forgot", controller.forgotPassword);
router.post("/changepassword", controller.changePassword);

module.exports = router;
