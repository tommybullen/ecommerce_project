const express = require("express");
const app = express();
require("dotenv").config();
const port = process.env.PORT || 3040;
const path = require("path");

app.use(require("express").urlencoded({ extended: true }));
app.use(require("express").json());

// ADMIN BRO
const AdminBro = require("admin-bro");
const AdminBroExpressjs = require("@admin-bro/express");
// We have to tell AdminBro that we will manage mongoose resources with it
AdminBro.registerAdapter(require("@admin-bro/mongoose"));

// require models
const Categories = require("./models/categories.models");
const Products = require("./models/products.models");
const Users = require("./models/users.models");
const Orders = require("./models/orders.models");
const adminBro = new AdminBro({
  resources: [Categories, Products, Users, Orders],
  rootPath: "/admin",
});
// Build and use a router which will handle all AdminBro routes
const router = AdminBroExpressjs.buildRouter(adminBro);
app.use(adminBro.options.rootPath, router);
// END ADMIN BRO

// =============== DATABASE CONNECTION =====================
const mongoose = require("mongoose");
async function connecting() {
  try {
    await mongoose.connect(process.env.MONGO, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });
    console.log("Connected to the DB");
  } catch (error) {
    console.log(
      "ERROR: Seems like your DB is not running, please start it up !!!"
    );
  }
}
connecting();
//================ CORS ================================
const cors = require("cors");
app.use(cors());
// =============== ROUTES ==============================
app.use("/users", require("./routes/users.routes"));
app.use("/products", require("./routes/products.routes"));
app.use("/payment", require("./routes/payment.routes"));
app.use("/orders", require("./routes/orders.routes"));
app.use("/emails", require("./routes/emails.routes"));

app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, "../client/build")));

app.get("/*", function (req, res) {
  res.sendFile(path.join(__dirname, "../client/build", "index.html"));
});

// =============== START SERVER =====================
app.listen(port, () => console.log(`server listening on port ${port}`));
