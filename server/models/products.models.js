const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  name: { type: String, unique: true, required: true },
  price: { type: Number, required: true },
  colour: { type: String, required: true },
  description: { type: String, required: true },
  image: { type: String, required: true },
  imagePublicId: {
    type: String,
    unique: true,
    required: true,
  },
  gender: { type: String, required: true },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "categories",
    required: true,
  },
  timesPurchased: { type: Number, default: 0 },
  dateCreated: { type: Date, default: Date.now() },
});

module.exports = mongoose.model("products", productSchema);
