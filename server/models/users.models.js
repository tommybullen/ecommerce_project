const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  email: { type: String, unique: true, required: true },
  password: { type: String, required: true },
  firstName: { type: String, required: false },
  lastName: { type: String, required: false },
  street: { type: String, required: false },
  town: { type: String, required: false },
  county: { type: String, required: false },
  postCode: { type: String, required: false },
  phoneNumber: { type: Number, required: false },
  admin: { type: Boolean, required: false },
  dateCreated: { type: Date, default: Date.now() },
});
module.exports = mongoose.model("users", userSchema);
