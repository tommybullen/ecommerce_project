const Orders = require("../models/orders.models");

const addOrder = async (req, res) => {
  console.log("reqbody :", req.body);

  try {
    await Orders.create(req.body);
    return res.json({
      ok: true,
      message: `Order added successfully`,
    });
  } catch (error) {
    res.json({ ok: false, error });
  }
};

const allOrders = async (req, res) => {
  try {
    const allOrders = await Orders.find({})
      .populate("userId")
      .populate("products.productId");
    res.send({ ok: true, data: allOrders });
  } catch (e) {
    res.send({ e });
  }
};

const userOrders = async (req, res) => {
  let id = req.params.id;
  console.log("REQ.PARAMS: ", req.params);
  try {
    const order = await Orders.find({ userId: id })
      .populate("userId")
      .populate("products.productId");
    res.send({ ok: true, data: order });
  } catch (e) {
    res.send({ e });
  }
};

module.exports = {
  addOrder,
  allOrders,
  userOrders,
};
