const Products = require("../models/products.models");
const Categories = require("../models/categories.models");
const cloudinary = require("cloudinary");
cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET,
});

const allProducts = async (req, res) => {
  try {
    const allProducts = await Products.find({}).populate("category");
    res.send({ ok: true, data: allProducts });
  } catch (e) {
    res.send({ e });
  }
};

const allCategories = async (req, res) => {
  try {
    const allCategories = await Categories.find({});
    res.send({ ok: true, data: allCategories });
  } catch (e) {
    res.send({ e });
  }
};

const addCategory = async (req, res) => {
  const { category } = req.body;
  try {
    const found = await Categories.findOne({ category });
    if (found) {
      return res.json({
        ok: false,
        message: `Category ${category} already exists`,
      });
    }
    await Categories.create({ category });
    return res.json({
      ok: true,
      message: `Category ${category} added successfully`,
    });
  } catch (error) {
    res.json({ ok: false, error });
  }
};

const addProduct = async (req, res) => {
  console.log("reqbody :", req);
  const { product } = req.body;
  console.log("product: ", product);
  try {
    const found = await Products.findOne({ name: product.name });
    if (found) {
      return res.json({
        ok: false,
        message: `Product ${product.name} already exists`,
      });
    }
    await Products.create(product);
    return res.json({
      ok: true,
      message: `Product ${product.name} added successfully`,
    });
  } catch (error) {
    res.json({ ok: false, error });
  }
};

const editCategory = async (req, res) => {
  let { initialValue, categoryValue } = req.body;
  console.log(req.body);
  try {
    await Categories.updateOne(initialValue, categoryValue);
    res.send({
      ok: true,
      data: `${initialValue.category} successfully updated to ${categoryValue.category}`,
    });
  } catch (err) {
    res.send({ ok: false, data: err });
  }
};

const editProduct = async (req, res) => {
  let { initialValues, editValues } = req.body;
  console.log(req.body);
  try {
    await Products.updateOne(initialValues, editValues);
    res.send({
      ok: true,
      data: `${editValues.name} successfully updated`,
    });
  } catch (err) {
    res.send({ ok: false, data: err });
  }
};

const deleteProduct = async (req, res) => {
  let { _id } = req.body;
  console.log(req.body);
  try {
    const deleted = await Products.findByIdAndRemove({ _id });
    if (deleted.imagePublicId) {
      await cloudinary.v2.api.delete_resources([deleted.imagePublicId]);
      res.send({
        ok: true,
        data: `Product and image deleted`,
      });
    } else {
      res.send({ ok: false, data: `Product deleted, image not found` });
    }
  } catch (err) {
    res.send({ ok: false, data: err });
  }
};

const deleteCategory = async (req, res) => {
  let { _id } = req.body;
  console.log(req.body);
  try {
    await Categories.deleteOne({ _id });
    await Products.deleteMany({ category: _id });
    res.send({
      ok: true,
      data: `Category and its products deleted`,
    });
  } catch (err) {
    res.send({ ok: false, data: err });
  }
};

const addSales = async (req, res) => {
  let { _id, quantity } = req.body;
  console.log("ID QUANTITY", _id, quantity);
  try {
    const res = await Products.findOneAndUpdate(
      { _id: _id },
      { $inc: { timesPurchased: quantity } }
    );
    console.log("res: ", res);
    res.send({ ok: true, data: `Sales updated` });
  } catch (err) {
    res.send({ ok: false, data: err });
  }
};

module.exports = {
  allProducts,
  addCategory,
  allCategories,
  addProduct,
  editCategory,
  editProduct,
  deleteProduct,
  deleteCategory,
  addSales,
};
