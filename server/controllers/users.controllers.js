const User = require("../models/users.models");
const argon2 = require("argon2"); //https://github.com/ranisalt/node-argon2/wiki/Options
const jwt = require("jsonwebtoken");
const validator = require("validator");
const jwt_secret = process.env.JWT_SECRET;
// the client is sending this body object
//  {
//     email: form.email,
//     password: form.password,
//     password2: form.password2
//  }
const register = async (req, res) => {
  const {
    email,
    password,
    password2,
    firstName,
    lastName,
    street,
    town,
    county,
    postCode,
    phoneNumber,
    admin,
  } = req.body;
  if (!email || !password || !password2)
    return res.json({ ok: false, message: "All fields required" });
  if (password !== password2)
    return res.json({ ok: false, message: "Passwords must match" });
  if (!validator.isEmail(email))
    return res.json({ ok: false, message: "Invalid credentials" });
  try {
    const user = await User.findOne({ email });
    if (user) return res.json({ ok: false, message: "Invalid credentials" });
    const hash = await argon2.hash(password);
    console.log("hash ==>", hash);
    const newUser = {
      email,
      password: hash,
      firstName,
      lastName,
      street,
      town,
      county,
      postCode,
      phoneNumber,
      admin,
    };
    await User.create(newUser);
    res.json({ ok: true, message: "Successfully registered" });
  } catch (error) {
    res.json({ ok: false, error });
  }
};
// the client is sending this body object
//  {
//     email: form.email,
//     password: form.password
//  }
const login = async (req, res) => {
  const { email, password } = req.body;
  if (!email || !password)
    return res.json({ ok: false, message: "All field are required" });
  if (!validator.isEmail(email))
    return res.json({ ok: false, message: "invalid data provided" });
  try {
    const user = await User.findOne({ email });
    if (!user) return res.json({ ok: false, message: "invalid data provided" });
    const match = await argon2.verify(user.password, password);
    if (match) {
      const token = jwt.sign(user.toJSON(), jwt_secret, { expiresIn: "1h" }); //{expiresIn:'365d'}
      if (user.admin) {
        res.json({
          ok: true,
          admin: true,
          message: `Welcome back ${user.firstName}. Redirecting to admin page.`,
          token,
          email,
          id: user._id,
        });
      }
      res.json({
        ok: true,
        admin: false,
        message: `Welcome back ${user.firstName}`,
        token,
        email,
        id: user._id,
      });
    } else return res.json({ ok: false, message: "Invalid data provided" });
  } catch (error) {
    res.json({ ok: false, error });
  }
};

const verify_token = (req, res) => {
  console.log("REQHEADERSAUTH: ", req.headers.authorization);
  const token = req.headers.authorization;
  jwt.verify(token, jwt_secret, (err, succ) => {
    err
      ? res.json({ ok: false, message: "Something went wrong", error: err })
      : res.json({ ok: true, succ });
  });
};

const forgotPassword = async (req, res) => {
  const { email } = req.body;
  try {
    const user = await User.findOne({ email });
    if (user) {
      const token = jwt.sign(user.toJSON(), jwt_secret, { expiresIn: "1h" });
      res.json({
        ok: true,
        token,
        email,
        id: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
      });
    } else return res.json({ ok: false, message: "Invalid data provided" });
  } catch (err) {
    res.json({ ok: false, err });
  }
};

const changePassword = async (req, res) => {
  const { _id, password, password2 } = req.body;
  if (password !== password2)
    return res.json({ ok: false, message: "Passwords must match" });
  const hash = await argon2.hash(password);
  try {
    const user = await User.findOneAndUpdate({ _id }, { password: hash });
    console.log(user);
    res.send({
      ok: true,
      user: user,
      message: `Password updated, please log in`,
    });
  } catch (err) {
    res.send({ ok: false, error: err });
  }
};

const all_users = async (req, res) => {
  try {
    const allUsers = await User.find({});
    res.send({ ok: true, data: allUsers });
  } catch (e) {
    res.send({ e });
  }
};

const findOne = async (req, res) => {
  console.log("reqPARAMS: ", req.params);
  let id = req.params.id;
  try {
    const user = await User.findOne({ _id: id });
    res.send({ ok: true, data: user });
  } catch (e) {
    res.send({ e });
  }
};

const edit_user = async (req, res) => {
  let { initialValues, editValues } = req.body;
  console.log(req.body);

  try {
    await User.updateOne(initialValues, editValues);
    res.send({
      ok: true,
      data: `User ${editValues.firstName} ${editValues.lastName} updated`,
    });
  } catch (err) {
    res.send({ ok: false, data: err });
  }
};

const deleteUser = async (req, res) => {
  let { _id } = req.body;
  console.log(req.body);
  try {
    await User.deleteOne({ _id });
    res.send({
      ok: true,
      data: `User deleted`,
    });
  } catch (err) {
    res.send({ ok: false, data: err });
  }
};

module.exports = {
  register,
  login,
  verify_token,
  all_users,
  edit_user,
  deleteUser,
  findOne,
  forgotPassword,
  changePassword,
};
