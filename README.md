# How to run it:

First clone the repo, then you will need 2 terminal windows.

In the first one you will run a client:

1) cd authentication_2020
2) cd client
3) npm install
4) npm start

In the second one you will run a server:

1) cd authentication_2020
2) cd server
3) npm install
4) nodemon index.js

If you are using windows and receive a Python related 
error after running npm install from the server folder,
run : 

```
npm install --global --production windows-build-tools
```
